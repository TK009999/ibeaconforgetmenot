import UIKit
import CoreLocation
import CoreBluetooth

public let iBeaconDataSheetURL: NSURL = NSURL(string: "https://script.google.com/macros/s/AKfycbyZADkZv52nAG2C78Wwho4tlG56N51Bj1O0mZ1WKx6weAq-_OzT/exec?no=5&location=%E7%AC%AC4%E5%8D%80&iBeaconID=5678-5678-0000-1212121212&DeviceModel=iphone6Plus")!

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var showNotification: Bool = false
    var howOftenOnceNotificaion: Int = 1 //可更改值，預設值每過3分鐘可重新收到一次通知
    let kShowDate: String = "kShowDate"
    let locationManager = CLLocationManager()
    
    let webView: UIWebView = UIWebView()
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        let uuidString = "E2C56DB5-DFFB-48D2-B060-D0F5A71096E0"
        let _Identifier = "MyBeacon"
        let _UUID: NSUUID = NSUUID(UUIDString: uuidString)!
        let _Major: CLBeaconMajorValue = 10
        let _Minor: CLBeaconMinorValue = 7
        let beaconRegion:CLBeaconRegion = CLBeaconRegion(proximityUUID: _UUID, major: _Major, minor: _Minor, identifier: _Identifier)
        beaconRegion.notifyOnEntry = true
        beaconRegion.notifyOnExit = true
        
        if(locationManager.respondsToSelector(#selector(CLLocationManager.requestAlwaysAuthorization))) {
            locationManager.requestAlwaysAuthorization()
        }
        
        locationManager.delegate = self
        locationManager.pausesLocationUpdatesAutomatically = false
        if #available(iOS 9.0, *) {
            locationManager.allowsBackgroundLocationUpdates = true
        } else {
            // Fallback on earlier versions
        }
        locationManager.startMonitoringForRegion(beaconRegion)
        //    locationManager.startRangingBeaconsInRegion(beaconRegion)
        //    locationManager.startUpdatingLocation()
        
        if(application.respondsToSelector(#selector(UIApplication.registerUserNotificationSettings(_:)))) {
            application.registerUserNotificationSettings(
                UIUserNotificationSettings(
                    forTypes: [UIUserNotificationType.Alert, UIUserNotificationType.Sound],
                    categories: nil
                )
            )
        }
        return true
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

// MARK: CLLocationManagerDelegate
extension AppDelegate: CLLocationManagerDelegate, CBPeripheralManagerDelegate {
    //  func locationManager(manager: CLLocationManager, didExitRegion region: CLRegion) {
    //    if let beaconRegion = region as? CLBeaconRegion {
    //      let notification = UILocalNotification()
    //      notification.alertBody = "Are you forgetting something?"
    //      notification.soundName = "Default"
    //      UIApplication.sharedApplication().presentLocalNotificationNow(notification)
    //    }
    //  }
    
    func sendLocalNotificationWithMessage(message: String!) {
        let notification:UILocalNotification = UILocalNotification()
        notification.alertBody = message
        notification.soundName = "tos_beep.caf";
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
        showNotification = false
        let nowDate: NSDate = NSDate()
        NSUserDefaults.standardUserDefaults().setValue(nowDate, forKey: kShowDate)
    }
    
    func locationManager(manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], inRegion region: CLBeaconRegion) {
        //        NSLog("didRangeBeacons");
        showNotification = false
        print("didRangeBeacons")
        var message:String = ""
        
        if(beacons.count > 0) {
            let nearestBeacon:CLBeacon = beacons[0]
            
            switch nearestBeacon.proximity {
            case CLProximity.Far:
                message = "You are far away from the beacon"
            case CLProximity.Near:
                showNotification = true
                message = "You are near the beacon"
            case CLProximity.Immediate:
                showNotification = true
                message = "You are in the immediate proximity of the beacon"
            case CLProximity.Unknown:
                return
            }
        } else {
            showNotification = true
            message = "No beacons are nearby"
        }
        
        if showNotification &&  self.comparisonTime() {
            NSLog("%@", message)
            sendLocalNotificationWithMessage(message)
        }
    }
    
    func comparisonTime() -> Bool {
        
        if let date: NSDate = NSUserDefaults.standardUserDefaults().valueForKey(kShowDate) as? NSDate {
            //            let dateformat = NSDateFormatter()
            //            dateformat.dateFormat = "HH:mm:ss"
            //            let nowDate: NSDate = NSDate()
            //            let pastDate = dateformat.stringFromDate(date)
            
            let calendar: NSCalendar = NSCalendar.currentCalendar()
            //設定日曆的時區，一定要設，否則返回時間會錯亂
            calendar.timeZone =  NSTimeZone(name: "Asia/Taipei")!
            //此為增加取得時間值類型，可以依需要添加
            let unitTypes: NSCalendarUnit = [NSCalendarUnit.Year, NSCalendarUnit.Month, NSCalendarUnit.Day, NSCalendarUnit.Weekday, NSCalendarUnit.Hour, NSCalendarUnit.Minute, NSCalendarUnit.Second]
            let past: NSDateComponents = calendar.components(unitTypes, fromDate: date)
            let nowDate: NSDate = NSDate()
            let now: NSDateComponents = calendar.components(unitTypes, fromDate: nowDate)
            print("now.minute - past.minute = \(now.minute) - \(past.minute) = \(now.minute - past.minute)")
            if (now.minute - past.minute) >= howOftenOnceNotificaion {
                return true
            } else {
                return false
            }
        } else {
            return true
        }
    }
    
    func locationManager(manager: CLLocationManager,
                         didEnterRegion region: CLRegion) {
        manager.startRangingBeaconsInRegion(region as! CLBeaconRegion)
        manager.startUpdatingLocation()
        
        simpleAlert("You entered the region", message: "")
        NSLog("You entered the region")
        sendLocalNotificationWithMessage("You entered the region")
        let beaconDataSheetURL: NSURL = NSURL(string: "https://script.google.com/macros/s/AKfycbyZADkZv52nAG2C78Wwho4tlG56N51Bj1O0mZ1WKx6weAq-_OzT/exec?no=5&location=%E7%AC%AC4%E5%8D%80&iBeaconID=5678-5678-0000-1212121212&DeviceModel=進入感應區")!
        webView.loadRequest(NSURLRequest(URL: beaconDataSheetURL))
    }
    
    func locationManager(manager: CLLocationManager,
                         didExitRegion region: CLRegion) {
        manager.stopRangingBeaconsInRegion(region as! CLBeaconRegion)
        manager.stopUpdatingLocation()
        
        simpleAlert("You exited the region", message: "")
        NSLog("You exited the region")
        sendLocalNotificationWithMessage("You exited the region")
        let beaconDataSheetURL: NSURL = NSURL(string: "https://script.google.com/macros/s/AKfycbyZADkZv52nAG2C78Wwho4tlG56N51Bj1O0mZ1WKx6weAq-_OzT/exec?no=5&location=%E7%AC%AC4%E5%8D%80&iBeaconID=5678-5678-0000-1212121212&DeviceModel=離開感應區")!
        webView.loadRequest(NSURLRequest(URL: beaconDataSheetURL))
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        switch status {
            
        case .AuthorizedAlways:
            // Starts the generation of updates that report the user’s current location.
            locationManager.startUpdatingLocation()
            
        case .Restricted:
            
            // Your app is not authorized to use location services.
            
            simpleAlert("Permission Error", message: "Need Location Service Permission To Access Beacon")
            
            
        case .Denied:
            
            // The user explicitly denied the use of location services for this app or location services are currently disabled in Settings.
            
            simpleAlert("Permission Error", message: "Need Location Service Permission To Access Beacon")
            
        default:
            // handle .NotDetermined here
            
            // The user has not yet made a choice regarding whether this app can use location services.
            break
        }
    }
    
    func simpleAlert(title:String, message: String) {
        
        let alert = UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: "OK")
        alert.show()
        
    }
    
    func locationManager(manager: CLLocationManager, didStartMonitoringForRegion region: CLRegion) {
        
        // Tells the delegate that a iBeacon Area is being monitored
        
        locationManager.requestStateForRegion(region)
    }
    
    func locationManager(manager: CLLocationManager, didDetermineState state: CLRegionState, forRegion region: CLRegion) {
        
        switch  state {
            
        case .Inside:
            //The user is inside the iBeacon range.
            
            locationManager.startRangingBeaconsInRegion(region as! CLBeaconRegion)
            
            break
            
        case .Outside:
            //The user is outside the iBeacon range.
            
            locationManager.stopRangingBeaconsInRegion(region as! CLBeaconRegion)
            
            break
            
        default :
            // it is unknown whether the user is inside or outside of the iBeacon range.
            break
            
        }
    }
    
    func peripheralManagerDidUpdateState(peripheral: CBPeripheralManager) {
        
        if peripheral.state == CBPeripheralManagerState.PoweredOff {
            
            simpleAlert("Beacon", message: "Turn On Your Device Bluetooh")
        }
    }
}
